local Logger = require("maple.classes.Logger")
local simpleFormat = require("maple.formatters.simple")
local consoleTransport = require("maple.transports.console")

---@class Maple : Maple.Logger.instance
local M = {}

M._VERSION = "0.4.1"

---@param options Maple.Logger.options
function M.createLogger(options)
	return Logger.new(options)
end

M.defaultLogger = Logger.new({
	formatters = { simpleFormat() },
	transports = { consoleTransport() },
})

setmetatable(M, {
	__index = function(t, k)
		local defaultLogger = rawget(t, "defaultLogger")
		if defaultLogger then
			return defaultLogger[k]
		end
	end,
})

return M
