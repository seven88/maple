---Open a log file for appending
---@param filepath string
---@param mode openmode
---@param message string
---@param level Maple.Level?
local function assertOpenFile(filepath, mode, message, level)
	local success, result = pcall(io.open, filepath, mode)
	if not success then
		error(string.format("%s:\n%s", message, result), level or 2)
	end

	return result
end

---Truncate a log file
---@param filepath string
---@param factor integer Truncation factor (e.g. 1 = 100%, 0.5 = 50%, etc.)
local function truncateFile(filepath, factor)
	local file = assertOpenFile(filepath, "r", "Failed to open file for reading for truncating", 3)

	local lines = {}
	for line in file:lines() do
		table.insert(lines, line)
	end
	file:close()

	local numLines = #lines
	local startLine = math.ceil(numLines * factor)

	if numLines > 0 then
		local file = assertOpenFile(filepath, "w", "Failed to open file for writing for truncating", 3)
		for i = startLine, numLines do
			file:write(lines[i] .. "\n")
		end
		file:close()
	end
end

---@class Maple.Transport.file.options : Maple.TransportFactory.options
---@field filepath string Path to log file
---@field softMaxFileSize integer? Max file size in bytes (not a hard limit but a best-effort target)
---@field truncationFactor integer? Truncation factor (e.g. 1 = 100%, 0.5 = 50%, etc.)

---@param options Maple.Transport.file.options?
return function(options)
	options = options or {}

	local file = assertOpenFile(options.filepath, "a", "Failed to open logfile")
	options.softMaxFileSize = options.softMaxFileSize or 50000
	options.truncationFactor = options.truncationFactor or 0.33

	if options.truncationFactor > 1 or options.truncationFactor <= 0 then
		error("Invalid truncation factor! Must be between 0 (exclusive) and 1 (inclusive)", 2)
	end

	---@type Maple.Transport
	return {
		level = options.level,
		formatters = options.formatters,

		---@param _ Maple.Logger.instance
		---@param msg Maple.LogMessage
		log = function(_, msg)
			local size = file:seek("end")

			if size > options.softMaxFileSize then
				file:close()
				truncateFile(options.filepath, options.truncationFactor)
				file = assertOpenFile(options.filepath, "a", "Failed to open logfile")
			end

			file:write(msg.message .. "\n")
			file:flush()
		end,
	}
end
