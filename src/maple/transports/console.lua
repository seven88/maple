---@param options Maple.TransportFactory.options?
return function(options)
	---@type Maple.Transport
	options = options or {}
	return {
		level = options.level,
		formatters = options.formatters,

		---@param _ Maple.Logger.instance
		---@param msg Maple.LogMessage
		log = function(_, msg)
			print(msg.message or msg._message)
		end,
	}
end
