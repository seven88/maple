local config = require("maple.config")

---@generic T
---@param t T
---@param seen table?
---@return T
local function copyTable(t, seen)
	if type(t) ~= "table" then
		return t
	end
	if seen and seen[t] then
		return seen[t]
	end

	local s = seen or {}
	local res = {}
	s[t] = res
	for k, v in pairs(t) do
		res[copyTable(k, s)] = copyTable(v, s)
	end
	return setmetatable(res, getmetatable(t))
end

---@generic T
---Assert an argument, raising an error with `message` at `level`.
---@param value T Value to assert. If falsy, will raise error
---@param message string Error message if assertion fails
---@param level Maple.Level Level to raise error at.
---@see error
---@return T
local function assertArg(value, message, level)
	if not value then
		error(message, level)
	end

	return value
end

---@class Maple.Logger
local M = {}

---Fields to deep copy when making a child logger. Fields not in the list will be shallow copied
M.deepCopyFields = { levels = true, formatters = true, transports = true, meta = true }

---@alias Maple.Level integer The integer value of a log level. Lower values are more important while higher values are more verbose.

---@class Maple.LogMessage
---@field _message string Raw message input (should not be mutated).
---@field message string? Message to be mutated by [formatters](lua://Maple.Formatter) and output by [transports](lua://Maple.Transport).
---@field level Maple.Level Log message level.
---@field meta table<any, any> Additional custom data.

---@class Maple.TransportFactory.options : table<any, any>
---@field formatters Maple.Formatter[]? [Formatters](lua://Maple.Formatter) for this [transport](lua://Maple.Transport) specifically. Overrides the formats defined in the parent logger
---@field level Maple.Level? Minimum log level. Any message with a greater numerical level will not be transported.

---@alias Maple.TransportFactory fun(options: Maple.TransportFactory.options?): Maple.Transport

---A `Transport` receives the current [logger](lua://Maple.Logger.instance) along with the [message](lua://Maple.LogMessage) to be logged, and "transports" it to some sort of output.
---@class Maple.Transport : Maple.TransportFactory.options
---@field log fun(logger: Maple.Logger.instance, msg: Maple.LogMessage)

---@alias Maple.FormatterFactory fun(options: any): Maple.Formatter

---A formatter receives a [`LogMessage`](lua://Maple.LogMessage) and mutates it. It can also return `false` to prevent the message being logged entirely.
---@alias Maple.Formatter fun(logger: Maple.Logger.instance, msg: Maple.LogMessage): false?

---@alias Maple.Logger.instance.log fun(self: Maple | Maple.Logger.instance, message: string, meta: table<any, any>?)
---@alias Maple.Logger.instance.logF fun(self: Maple | Maple.Logger.instance, meta: table<any, any>, format: string, ...: any) | fun(self: Maple | Maple.Logger.instance, format: string, ...: any)

---@class Maple.Logger.instance : Maple.Logger.options
---@field new fun(options: Maple.Logger.options)
---@field log fun(self: Maple.Logger.instance, level: Maple.Level, message: string, meta: table<any, any>?)
---@field logF fun(self: Maple.Logger.instance, level: Maple.Level, meta: table<any, any>, format: string, ...: any) | fun(self: Maple.Logger.instance, level: Maple.Level, format: string, ...: any)
---@field protect fun(self: Maple.Logger.instance, fn: function, ...: any): boolean, ...
---@field child fun(self: Maple.Logger.instance, meta: table<any, any>): Maple.Logger.instance
---@field [string] Maple.Logger.instance.log | Maple.Logger.instance.logF
M.prototype = {}

---@class Maple.LevelDefinition
---@field levels Maple.config.levels Mapping of level names to numerical values (like an enum)
---@field names table<Maple.Level, string> Mapping of level values to display names
---@field colors table<Maple.Level, string> Mapping of level values to colors

---@class Maple.Logger.options
---Transports to send log messages to. They are run in order and "transport" log messages to some output.
---
---
---Maple comes with [some transports](https://gitlab.com/seven88/maple/-/blob/main/doc/transports.md), but you can also create your own [custom transports](https://gitlab.com/seven88/maple#custom-transports) for your specific use case.
---
---A [transport](lua://Maple.Transport) can be added to a logger the idiomatic Lua way:
---```lua
---table.insert(myLogger.transports, myTransport)
---```
---@field transports Maple.Transport[]
---[Formatters](lua://Maple.Formatter) mutate log messages in order to format them. They are run in the given order on each log message before sending to [transports](lua://Maple.Transport) for writing.
---
---Maple comes with [some formatters](https://gitlab.com/seven88/maple/-/blob/main/doc/formatters.md), but you can also create your own [custom formatters](https://gitlab.com/seven88/maple#custom-formatters) for your specific use case.
---
---A formatter can be added to a logger the idiomatic Lua way:
---```lua
---table.insert(myLogger.formatters, 1, myFormatter) -- insert into first position to be run first
---```
---@field formatters Maple.Formatter[]
---@field levels Maple.LevelDefinition? The levels (containing names and colors) to use for logging. If omitted, the [default](lua://Maple.config.levels) will be used.
---@field level Maple.Level? The minimum (inclusive) level a message must be to be logged.
---@field meta table<any, any>? Metadata to apply to all messages logged by this logger.
---@field shutup boolean? Silence this logger. When `true`, nothing will be logged.

---Create a new [`Maple.Logger`](lua://Maple.Logger.instance).
---@param options Maple.Logger.options
---@return Maple.Logger.instance
function M.new(options)
	local self = setmetatable(options, { __index = M.prototype }) --[[@as Maple.Logger.instance]]

	-- If a minimum level was not provided, log everything
	self.level = self.level or math.huge

	assertArg(self.transports and next(self.transports), "At least one transport must be provided", 2)
	assertArg(self.formatters and next(self.formatters), "At least one formatter must be provided", 2)
	assertArg(self.meta == nil or type(self.meta) == "table", "Provided metadata must be a table or nil", 2)

	if not self.levels then
		self.levels = {
			levels = config.levels,
			names = config.levelNames,
			colors = config.levelColors,
		}
	end

	assertArg(type(self.levels.levels) == "table", "levels.levels must be a table", 2)
	assertArg(type(self.levels.names) == "table", "levels.names must be a table", 2)
	assertArg(type(self.levels.colors) == "table", "levels.colors must be a table", 2)

	-- check for reserved level words
	for k, _ in pairs(self.levels.levels) do
		if self[k] then
			error(string.format("Log level %q is a reserved word. Please choose another name for the level", k), 2)
		end
	end

	-- add method for each logging level
	---@param levelName string
	---@param levelValue Maple.Level
	for levelName, levelValue in pairs(self.levels.levels) do
		self[levelName] = function(logger, message, meta)
			logger:log(levelValue, message, meta)
		end
		---@param logger Maple.Logger.instance
		---@param meta table<any, any>
		---@param format string
		---@param ... any
		self[levelName .. "F"] = function(logger, meta, format, ...)
			logger:logF(levelValue, meta, format, ...)
		end
	end

	return self
end

---Log a message, sending it to any enabled [formatters](lua://Maple.Formatter), and finally [transporters](lua://Maple.Transport).
---@param level Maple.Level Log level
---@param message any
---@param meta table<any, any>?
function M.prototype:log(level, message, meta)
	if self.shutup or level > self.level then
		return
	end

	---@type Maple.LogMessage
	local logMessage = {
		_message = message,
		message = "",
		level = level,
		meta = {},
	}

	if meta ~= nil and type(meta) ~= "table" then
		error("Provided metadata is not a table", 1)
	end

	---@param msg Maple.LogMessage
	---@param meta table<any, any>?
	local function applyMeta(msg, meta)
		for k, v in pairs(meta or {}) do
			msg.meta[k] = v
		end
	end

	applyMeta(logMessage, self.meta)
	applyMeta(logMessage, meta)

	---@param logMessage Maple.LogMessage
	---@param formatters Maple.Formatter[]
	local function applyFormatters(logMessage, formatters)
		for i, formatter in ipairs(formatters) do
			local success, result = pcall(formatter, self, logMessage)
			if not success then
				error(string.format("Formatter %i encountered an error:\n%s", i, result), 3)
			end

			if result == false then
				return false
			end
		end
	end

	if applyFormatters(logMessage, self.formatters) == false then
		return
	end

	for i, transport in ipairs(self.transports) do
		if (transport.level or math.huge) >= logMessage.level then
			assertArg(transport.log, string.format("Transport %i does not have a log method!", i), 2)

			local msg = logMessage

			-- If a transport has specified its own formatters, apply them
			if transport.formatters then
				local msgCopy = {}

				for k, v in pairs(logMessage) do
					msgCopy[k] = v
				end

				msgCopy.message = ""
				msg = msgCopy

				if applyFormatters(msgCopy, transport.formatters) == false then
					return false
				end
			end

			local success, result = pcall(transport.log, self, msg)
			if not success then
				error(string.format("Transport %i encountered an error:\n%s", i, result), 2)
			end
		end
	end
end

---Log a message that is string.format'd
---@param level Maple.Level Log level
---@param meta table<any, any>
---@param format string
---@param ... any
---@overload fun(self: Maple.Logger.instance, level: Maple.Level, format: string, ...: any)
function M.prototype:logF(level, meta, format, ...)
	local args = { ... }

	-- If no meta given, assume meta argument is actually format string
	if type(meta) ~= "table" then
		table.insert(args, 1, format)
		format = meta
		meta = nil
	end

	local success, result = pcall(string.format, format, table.unpack(args))

	if not success then
		local err = string.format(
			"String formatting error: %s\nFormat string: %q\nArgs:\n  %s",
			result,
			format,
			table.concat(args, "\n  ")
		)
		error(err, 1)
	end

	self:log(level, result, meta)
end

---Call a function in protected mode so that errors do not propagate, instead being caught and logged.
---@param prefix string Text to prefix the error with. A space will be inserted between this prefix and the error message. Defaults to "Caught error:".
---@param fn function Function to call in protected mode
---@param ... any Arguments to pass to `fn`
---@return boolean success Whether the function succeeded or encountered an error
---@return any ... Values returned from the function
---@overload fun(self: Maple.Logger.instance, fn: function, ...: any): boolean, any
function M.prototype:protect(prefix, fn, ...)
	local msgPrefix ---@type string
	local func ---@type function
	local args = {}

	local firstArgType = type(prefix)

	if firstArgType == "function" then
		msgPrefix = "Caught error:"
		func = prefix
		args = { fn, ... }
	elseif firstArgType == "string" then
		msgPrefix = prefix
		func = fn
		args = { ... }
		if type(fn) ~= "function" then
			error("Argument #2 must be a function when providing a error message prefix", 1)
		end
	else
		error("Argument #1 must be an error message prefix or function to call in protected mode", 1)
	end

	---backwards compatible unpack
	---@diagnostic disable-next-line:deprecated
	local uPack = table.unpack or unpack

	local results = { pcall(func, uPack(args)) }

	if results[1] == false then
		local loggingLevel = self.levels.levels.error or 1
		self:logF(loggingLevel, msgPrefix .. " %s", tostring(results[2]))
		return uPack(results)
	else
		return uPack(results)
	end
end

---Perform a performance profiling.
---@param level Maple.Level Level to log at
---@param msg string? Message to prefix the duration with. Defaults to "Execution duration: ".
---@return fun() stop A function that when called will log the duration since calling `profile`.
function M.prototype:profile(level, msg)
	msg = msg or "Execution duration: "

	local start = os.clock()

	return function()
		self:logF(level, msg .. "%fs", os.clock() - start)
	end
end

---Create a child logger from another logger.
---@param meta table<any, any>?
---@return Maple.Logger.instance
function M.prototype:child(meta)
	local child = setmetatable({}, { __index = M.prototype })

	local parent = self.defaultLogger and self.defaultLogger or self --[[@as Maple.Logger.instance]]

	---@param k any
	---@param v any
	for k, v in pairs(parent) do
		if M.deepCopyFields[k] then
			child[k] = copyTable(v)
		else
			child[k] = v
		end
	end

	if not child.meta and meta then
		child.meta = {}
	end

	for k, v in pairs(meta or {}) do
		child.meta[k] = v
	end

	return child
end

return M
