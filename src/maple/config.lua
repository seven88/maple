---@class Maple.config
local M = {}

---@class Maple.config.levels
M.levels = {
	error = 1,
	warn = 2,
	info = 3,
	debug = 4,
	trace = 5,
}

M.levelNames = {
	"Error",
	"Warn",
	"Info",
	"Debug",
	"Trace",
}

M.levelColors = {
	"\27[38;2;255;0;0m",
	"\27[38;2;255;180;0m",
	"\27[38;2;0;255;120m",
	"\27[38;2;0;255;255m",
	"\27[38;2;120;120;120m",
}

return M
