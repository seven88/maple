return function()
	---@param _ Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(_, message)
		if message.meta.stacktrace == true then
			local stack = debug.traceback(nil, 4)
			message.message = message.message .. string.format("\n%s", stack)
		end
	end
end
