---@param format string?
local function getDateTimeStamp(format)
	local current_time = os.time()
	return os.date(format or "%Y-%m-%dT%H:%M:%S", current_time)
end

---@class Maple.Formatter.timestamp.options
---@field format string? Date format string

---@param options Maple.Formatter.timestamp.options?
return function(options)
	options = options or {}

	---@param _ Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(_, message)
		message.message = message.message .. string.format("%s", getDateTimeStamp(options.format))
	end
end
