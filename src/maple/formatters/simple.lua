local timestampFormatter = require("maple.formatters.timestamp")

---@class Maple.Formatter.simple.options
---@field levelLength integer? Should be the length of your longest level name. Defaults to 5

---@param options Maple.Formatter.simple.options?
return function(options)
	local timestamp = timestampFormatter()

	options = options or {}
	if type(options) ~= "table" then
		error("options argument (#1) must be a table", 2)
	end

	options.levelLength = options.levelLength or 5
	if type(options.levelLength) ~= "number" then
		error("levelLength option must be a number", 2)
	end

	---@param logger Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(logger, message)
		timestamp(logger, message)

		local lvlName = logger.levels.names[message.level]
		local lvlPadding = string.rep(" ", options.levelLength - #lvlName)

		message.message = message.message .. string.format(" | %s%s | ", lvlName, lvlPadding)
		message.message = message.message .. message._message
	end
end
