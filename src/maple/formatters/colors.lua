return function()
	---@param logger Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(logger, message)
		local color = logger.levels.colors[message.level]
		message.message = message.message .. color
	end
end
