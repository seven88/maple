return function()
	---@param _ Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(_, message)
		if next(message.meta) then
			message.message = message.message .. " {"
			local hasRun = false
			for k, v in pairs(message.meta) do
				local kv = string.format("%s=%s", (hasRun and ", " or "") .. tostring(k), tostring(v))
				message.message = message.message .. kv
				hasRun = true
			end
			message.message = message.message .. "}"
		end
	end
end
