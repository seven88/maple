return function()
	---@param _ Maple.Logger.instance
	---@param message Maple.LogMessage
	return function(_, message)
		message.message = message.message .. "\27[38;2;255;255;255m"
	end
end
