# Maple Lua

![Example logger output](doc/images/logging_demo.png)

**Maple** is designed to be a simple yet extensible logging library for Lua. It is inspired by [WinstonJS](https://github.com/winstonjs/winston), and thus, shares many of the same features, while being Lua-ified.

## Install

Maple can be installed via LuaRocks:

```sh
luarocks install maple
```

## TL;DR

```lua
local maple = require("maple")

-- Use the default logger with default levels
maple:error("Something has gone terribly wrong")
maple:warn("Something is about to go terribly wrong")
maple:info("Things are alright")
maple:debug("Things have probably gone wrong")
maple:trace("Things have definitely gone wrong")

-- Format a string
maple:infoF("I am number %i", 1)

-- Attach a new message formatter to default logger
local metaFormatter = require("maple.formatter.meta")
table.insert(maple.defaultLogger.formatters, metaFormatter)

-- Now additional metadata can be logged
maple:debug("What can metadata be?", { any = "thing!" })

-- When using the -F (formatting) variant, the metadata must be the first argument.
maple:debugF({ adminMode = true }, "If it can be tostring()'d, %s",  "it can be metadata")
maple:debugF("But metadata is still %s", "optional")


-- Create your own logger
local colorFormatter = require("maple.formatters.colors")
local simpleFormatter = require("maple.formatters.simple")
local fileTransport = require("maple.transports.file")

local logger = maple.createLogger({
	formatters = { colorFormatter(), simpleFormatter() },
	transports = { fileTransport({ filepath = "app.log" }) },
	levels = {
		levels = {
			urgent = 1,
			high = 2,
			medium = 3,
			low = 4,
		},
		names = {
			"URGENT",
			"High",
			"Medium",
			"Low",
		},
		colors = {
			"\27[38;2;180;0;0m",
			"\27[38;2;255;80;0m",
			"\27[38;2;255;255;0m",
			"\27[38;2;0;0;255m",
		},
	},
})

logger:urgent("Things have gone south")

-- Overwrite the default logger to be used application-wide
maple.defaultLogger = logger
```

## Logger

A logger object is what you, the user, will interact with directly. A logger can be created using <code>createLogger(<a href="#logger-options">options</a>)</code> (from [`init.lua`](src/init.lua)), or you can just use the [default logger](#default-logger):

```lua
local maple = require("maple")

local logger = maple.createLogger(options)
local defaultLogger = maple

logger:debug("This is a test!")
defaultLogger:warn("I'm warning you!")
```

### log()

A logger contains a method; `logger:log(level, message, meta)` that allows you to send log messages to [transports](#transports) where they will, presumably, be readable by you! It will also contain a method, per level that you define (in the [`options.levels`](#logger-options) field), and expose it. For example, if you [define](#custom-levels) a [`wumbo`](https://youtu.be/P1O_cR8WUYA) level, you will find a convenient `logger.wumbo` method for logging messages at the `wumbo` level:

```lua
local maple = require("maple")

local simpleFormatter = require("maple.formatters.simple")
local consoleTransport = require("maple.transports.console")

local logger = maple.createLogger({
	formatters = { simpleFormatter() },
	transports = { consoleTransport() },
	levels = {
		levels = {
			wumbo = 1,
			mini = 2,
		},
		names = {
			"wumbo",
			"mini",
		},
		colors = {
			"\27[38;2;255;0;255m",
			"\27[38;2;255;0;0m",
		},
	},
})

logger:wumbo("Studying wumbo...")
```

### logF()

Loggers also have a `logger:logF(level, meta?, format, ...)` method that allows you to format a message (like `string.format`) before logging it. It too is used for wrapping with custom levels. Continuing the above example;

```lua
local username = "patrick*"
logger:wumboF({ user = 1234 }, "User %s has joined", username)

-- Metadata can also be omitted if not needed
logger:wumboF("User %s has been kicked", username)
```

### protect()

The `logger:protect(fn, ...)` method lets you easily call a function in [protected mode](https://www.lua.org/manual/5.4/manual.html#2.3) so that errors are not propagated, but instead logged.

```lua
local success, result = logger:protect(function()
	error("Something went wrong!")
end) -- logs "Caught error: Something went wrong!"

local success, result = logger:protect("Deleting user failed:", function(user)
	error("User not found!")
end, user) -- logs "Deleting user failed: User not found!"
```

### profile()

The `logger:profile(level, msg?): stop` method can be used to profile execution durations. It uses [`os.clock`](https://www.lua.org/manual/5.3/manual.html#pdf-os.clock) under the hood to get an approximation of CPU time used by the program in seconds.

```lua
local stop = logger:profile(logger.levels.levels.debug, "Execution took: ")

local x = 80

for i=1, 50 do
	x = x / 2
end

stop() -- execution duration will be logged
```

### child()

Create a child logger from another logger. A child has the same functionality as their parent but can contain different [levels](#levels), [formatters](#formatters), [transports](#transports), and metadata.

```lua
local metaFormatter = require("maple.formatters.meta")

local requestLogger = parentLogger:child({ requestID = 1234 })

table.insert(requestLogger.formatters, metaFormatter())

requestLogger:trace("Start handling request")
```

### Logger Options

When creating a [logger](#logger), you must pass an “options” `table` argument that is used for configuring it. The table should contain the following:

|   Field    |                   Value Type                    | Required |                                                                                                        |
| :--------: | :---------------------------------------------: | :------: | ------------------------------------------------------------------------------------------------------ |
| transports |     <code>[Transport](#transports)[]</code>     |    ✅    | Transporters to send messages to.                                                                      |
| formatters |     <code>[Formatter](#formatters)[]</code>     |    ✅    | Formatters to run on messages (in given order) before sending to transports.                           |
|   levels   | <code>[level definition](#custom-levels)</code> |    ❌    | Logging levels to use. If omitted, the [default levels](#level-defaults) will be used.                 |
|   level    |                    `integer`                    |    ❌    | The minimum (inclusive) level a message must be to be logged. If omitted, all messages will be logged. |
|    meta    |                     `table`                     |    ❌    | Additional metadata that could be printed. All contents should be `tostring()`-able.                   |
|   shutup   |                    `boolean`                    |    ❌    | If `true`, this logger will not log anything.                                                          |

## Default Logger

The default [logger](#logger) is set up like so:

```lua
Logger.new({
	formatters = { simpleFormat() },
	transports = { consoleTransport() },
})
```

Notice that is does not specify a `level` field. As [described above](#logger-options), this means the [default levels](#level-defaults) will be used. It can be used like so:

```lua
local maple = require("maple")

maple:warn("I'm warning you!")

--[[ Console Output:
2024-02-03T23:59:59 | Warn  | I'm warning you!
]]
```

It can be [customized](#customizing-a-logger) like any logger, or completely replaced:

```lua
local maple = require("maple")

local colorsFormatter = require("maple.formatters.colors")
local simpleFormat = require("maple.formatters.simple")
local consoleTransport = require("maple.transports.console")

-- Replace the default logger with our own
maple.defaultLogger = maple.createLogger({
	formatters = { colorsFormatter(), simpleFormat() },
	transports = { consoleTransport() },
})
```

## Customizing a logger

There is minimal “lua magic” being done, so modifying an existing logger is pretty intuitive and idiomatic – take adding a color formatter to the default logger as an example:

```lua
local maple = require("maple")
local colorsFormatter = require("maple.formatters.colors")

table.insert(maple.defaultLogger.formatters, 1, colorsFormatter())
```

## Levels

Logging levels are the core of any logger and nothing is more frustrating than being forced to use levels that don't make sense for your use case. With Maple, you can [customize](#custom-levels) the levels to your liking, or just use the [defaults](#level-defaults).

### Custom Levels

Custom levels can easily be defined and used by any [logger](#logger):

```lua
local maple = require("maple")

-- We call this a "level definition".
-- Loggers need one and it must be this "shape".
local levels = {
	levels = {
		one = 1, -- levels should always start at 1!
		two = 2,
		three = 3,
	},
	names = {
		"one",
		"two",
		"three",
	},
	colors = {
		"\27[38;2;255;0;0m",
		"\27[38;2;0;255;0m",
		"\27[38;2;0;0;255m",
	},
}

-- Feel free to throw your custom levels in the main maple module if you'd like.
-- Then they can be accessed wherever the main module can be.
maple.levels = levels

maple.defaultLogger.levels = levels
```

### Level Defaults

The default levels are used whenever the [`options.levels`](#customizing-a-logger) field is omitted. They are defined in [`config.lua`](src/config.lua) and are the common five:

| Level | Value | Name  |          Color          |
| :---: | :---: | :---: | :---------------------: |
| Error |   1   | Error |   255,0,0 (`#ff0000`)   |
| Warn  |   2   | Warn  |  255,180,0 (`#ffb400`)  |
| Info  |   3   | Info  |  0,255,120 (`#00ff78`)  |
| Debug |   4   | Debug |  0,255,255 (`#00ffff`)  |
| Trace |   5   | Trace | 120,120,120 (`#787878`) |

## Formatters

Formatters allow users to customize the formatting of log messages. A formatter receives a [`LogMessage`](#the-logmessage) and mutates it – or can even prevent it from being logged at all. **Formatters are run sequentially**, meaning if `colors` is run last, no text will be colored. Likewise, it can be placed between other formatters to color half a message.

### Included Formatters

There are many formatters that are included:

- [`colors`](doc/formatters.md#colors)
- [`meta`](doc/formatters.md#meta)
- [`simple`](doc/formatters.md#simple)
- [`stacktrace`](doc/formatters.md#stacktrace)
- [`timestamp`](doc/formatters.md#timestamp)

You can read more about them in [`doc/formatters.md`](doc/formatters.md).

### Custom Formatters

A formatter is a [closure](<https://en.wikipedia.org/wiki/Closure_(computer_programming)>) that can accept options and must return a function that receives the parent [`logger`](#logger) and [`LogMessage`](#the-logmessage). It can then mutate the message or even return `false` to prevent it from being logged entirely. Here is a relatively simple example formatter, “filter”, that filters out logs if their level appears in the “denied” `table`, and otherwise adds the level name to the message:

```lua
-- Filter formatter
return function(options)
	return function(logger, message)
		for _, lvl in ipairs(options.denied) do
			if message.level == lvl then
				return false
			end
		end

		local levelName = logger.levels.names[message.level]
		message.message = message.message .. levelName
	end
end
```

It can then be used like so:

```lua
local maple = require("maple")

local filter = require("filterFormatter")
local consoleTransport = require("maple.transports.console")

local levels = {
	levels = {
		one = 1,
		two = 2,
		three = 3,
	},
	names = {
		"one",
		"two",
		"three",
	},
	colors = {
		"\27[38;2;255;0;0m",
		"\27[38;2;0;255;0m",
		"\27[38;2;0;0;255m",
	},
}

local logger = maple.createLogger({
	levels = levels,
	formatters = { filter({ denied = { levels.levels.two } }) },
	transports = { consoleTransport() },
})

logger:one("This will be logged")
logger:two("This will not")
```

## Transports

A transport is a table with a simple job; take a message, and “transport” it to some sort of output (console, log file, etc.).

Every transport can accept an options `table` that specifies a [level](#levels) and [formatters](#formatters) to use instead of the ones defined in the parent [logger's options](#logger-options):

|   Field    |               Value Type                |
| :--------: | :-------------------------------------: |
|   level    |                `integer`                |
| formatters | <code>[Formatter](#formatters)[]</code> |

```lua
local maple = require("maple")
local config = require("maple.config")

local myLogger = maple.createLogger({
	formatters = { colorsFormatter(), simpleFormat() },
	transports = { consoleTransport({ level = config.levels.error, formatters = { simpleFormat() } }) },
})

myLogger:error("No colors here! Not even a colour!")
```

### Included Transports

There are a couple transports that come “in the box”:

- [`console`](doc/transports.md#console)
- [`file`](doc/transports.md#file)

You can read more about them in [`doc/transports.md`](doc/transports.md).

### Custom Transports

A custom transport will let you, for example, send log messages to a webserver, database, or other. They do this by providing a `log` method in a table. Here is an example that sends log messages to a URL:

```lua
return function(options)
	-- This is essential, even if you don't use any options in your log
	-- function, as it prevents nil references below
	options = options or {}

	return {
		-- This and the "formatters" field below are needed in order to allow
		-- overriding of the parent logger's values. Do NOT forget them!
		level = options.level,
		formatters = options.formatters,

		log = function(logger, msg)
			http.post("https://example.com/logs/add", {
				-- adding the "or msg._message" can be a nice
				-- fallback to print the raw input, should
				-- formatting go terribly wrong
				message = msg.message or msg._message,
				level = msg.level,
				metadata = msg.meta,
			})
			print(msg.message or msg._message)
		end,
	}
end
```

Now we can use the above transport to send error messages to the server:

```lua
local maple = require("maple")

local simpleFormat = require("maple.formatters.simple")
local httpTransport = require("httpTransport")

local levels = {
	levels = {
		error = 1,
		warn = 2,
		whatever = 3,
	},
	names = {
		"error",
		"warn",
		"whatever",
	},
	colors = {
		"\27[38;2;255;0;0m",
		"\27[38;2;255;255;0m",
		"\27[38;2;0;255;0m",
	},
}

local logger = maple.createLogger({
	levels = levels,
	formatters = { simpleFormat() },
	transports = { httpTransport({level = levels.levels.error}) },
})

logger:error("This will be sent to our website!")
logger:warn("This will not")
```

## The `LogMessage`

At the core of the logger is the `LogMessage` object. When a [`logger`](#logger) is requested to `log()` a message, the logger constructs a `LogMessage` to be passed to [formatters](#formatters) and [transports](#transports).

|   Field   |   Type    |                                                                                                         |
| :-------: | :-------: | ------------------------------------------------------------------------------------------------------- |
| \_message | `string`  | The raw message that was given to `logger.log`. Do **not** mutate.                                      |
|  message  | `string`  | The message that is to be mutated by [formatters](#formatters) and logged by [transports](#transports). |
|   level   | `integer` | The [level](#levels) of this message.                                                                   |
|   meta    |  `table`  | Additional metadata about the message that could be printed. All contents should be `tostring()`-able.  |
