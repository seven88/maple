local metaFormatter = require("maple.formatters.meta")

describe("meta formatter", function()
	it("appends metadata", function()
		local f = metaFormatter()

		local logger = {}
		local message = { level = 1, _message = "Testing", message = "Prefix", meta = { val = "data", number = 78 } }

		f(logger, message)

		assert.equal("Prefix {", string.sub(message.message, 1, 8))
		local value = string.sub(message.message, 9, 27)
		assert.True(value == "val=data, number=78" or value == "number=78, val=data")
		assert.equal("}", string.sub(message.message, 28))
	end)
end)
