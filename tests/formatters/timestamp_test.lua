local timestampFormatter = require("maple.formatters.timestamp")

describe("timestamp formatter", function()
	local originalDate = os.date

	setup(function()
		os.date = spy.new(function(format)
			return format
		end)
	end)
	teardown(function()
		os.date = originalDate
	end)

	it("adds the current date/timestamp", function()
		local f = timestampFormatter()

		local logger = {}
		local message = { level = 2, _message = "Testing", message = "Prefix" }

		f(logger, message)

		assert.equal("Prefix%Y-%m-%dT%H:%M:%S", message.message)
	end)

	it("allows passing a custom format string", function()
		local f = timestampFormatter({ format = "%Y%M%D" })

		local logger = {}
		local message = { level = 1, _message = "Testing", message = "Prefix" }

		f(logger, message)

		assert.equal("Prefix%Y%M%D", message.message)
	end)
end)
