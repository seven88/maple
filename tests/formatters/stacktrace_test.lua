local stacktraceFormatter = require("maple.formatters.stacktrace")

describe("stacktrace formatter", function()
	local originalTraceback = debug.traceback

	setup(function()
		debug.traceback = spy.new(function()
			return "insert_stack_here"
		end)
	end)
	teardown(function()
		debug.traceback = originalTraceback
	end)
	after_each(function()
		debug.traceback:clear()
	end)

	it("adds a stacktrace when meta.stacktrace is true", function()
		local f = stacktraceFormatter()

		local logger = {}
		local message = { level = 2, _message = "Testing", message = "Prefix", meta = { stacktrace = true } }

		f(logger, message)

		assert.stub(debug.traceback).called_with(nil, 4)
		assert.equal("Prefix\ninsert_stack_here", message.message)
	end)

	it("does not add a stacktrace when meta.stacktrace is not true", function()
		local f = stacktraceFormatter()

		local logger = {}
		local message = { level = 2, _message = "Testing", message = "Prefix", meta = {} }

		f(logger, message)

		assert.stub(debug.traceback).called(0)
		assert.equal("Prefix", message.message)
	end)
end)
