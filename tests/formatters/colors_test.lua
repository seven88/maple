local colorsFormatter = require("maple.formatters.colors")

describe("colors formatter", function()
	it("adds colours", function()
		local f = colorsFormatter()

		local logger = { levels = { colors = { "abc", "def" } } }
		local message = { level = 2, _message = "Testing", message = "Prefix" }

		f(logger, message)

		assert.equal("Prefixdef", message.message)
	end)
end)
