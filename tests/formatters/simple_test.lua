local simpleFormatter = require("maple.formatters.simple")

describe("simple formatter", function()
	local originalDate = os.date

	setup(function()
		os.date = spy.new(function()
			return "2024-2-3"
		end)
	end)
	teardown(function()
		os.date = originalDate
	end)

	it("throws if options is not a table", function()
		assert.error(function()
			---@diagnostic disable:param-type-mismatch
			simpleFormatter(true)
		end, "options argument (#1) must be a table")
	end)

	it("throws if options.levelLength is not a number", function()
		assert.error(function()
			---@diagnostic disable:assign-type-mismatch
			simpleFormatter({ levelLength = true })
		end, "levelLength option must be a number")
	end)

	it("formats the log message", function()
		local f = simpleFormatter()

		local logger = { levelLength = 3, levels = { names = { "one", "two" } } }
		local message = { level = 1, _message = "Testing", message = "Prefix" }

		f(logger, message)

		assert.equal("Prefix2024-2-3 | one   | Testing", message.message)
	end)
end)
