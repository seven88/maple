local uncolorsFormatter = require("maple.formatters.uncolor")

describe("uncolor formatter", function()
	it("removes colours", function()
		local f = uncolorsFormatter()

		local logger = { levels = { colors = { "abc", "def" } } }
		local message = { level = 2, _message = "Testing", message = "Prefix" }

		f(logger, message)

		assert.equal("Prefix\27[38;2;255;255;255m", message.message)
	end)
end)
