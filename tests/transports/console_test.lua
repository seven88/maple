local consoleTransport = require("maple.transports.console")

describe("console transport", function()
	it("passes through level override", function()
		local transport = consoleTransport({ level = 16 })

		assert.equal(16, transport.level)
	end)

	it("passes through formatters override", function()
		local formatters = {}
		local transport = consoleTransport({ formatters = formatters })

		assert.equal(formatters, transport.formatters)
	end)

	describe("transports a message", function()
		setup(function()
			stub(_G, "print")
		end)

		teardown(function()
			---@diagnostic disable-next-line:undefined-field
			print:revert()
		end)

		it("using print", function()
			local transport = consoleTransport()

			local logMessage = { message = {} }
			transport.log({}, logMessage) ---@diagnostic disable-line:missing-fields

			assert.stub(print).called_with(match.is_ref(logMessage.message))
		end)
	end)
end)
