local fileTransport = require("maple.transports.file")

describe("file transport", function()
	setup(function()
		mock(io, true)
	end)

	teardown(function()
		mock.revert(io)
	end)

	describe("receives options", function()
		describe("and passes through", function()
			it("level override", function()
				---@diagnostic disable-next-line:missing-fields
				local transport = fileTransport({ level = 16 })

				assert.equal(16, transport.level)
			end)

			it("formatters override", function()
				local formatters = {}
				---@diagnostic disable-next-line:missing-fields
				local transport = fileTransport({ formatters = formatters })

				assert.equal(formatters, transport.formatters)
			end)
		end)

		it("and opens the logfile for writing", function()
			fileTransport({ filepath = "some/file.log" })

			assert.stub(io.open).called_with("some/file.log", "a")
		end)

		describe("and throws if", function()
			it("the log file cannot be opened", function()
				local oldOpen = io.open
				io.open = spy.new(function()
					error("Failed to open file due to reasons")
				end)

				assert.error_match(function()
					fileTransport({ filepath = "some/file.log" })
				end, "Failed to open logfile:\n[^:]+:%d+: Failed to open file due to reasons")

				io.open = oldOpen
			end)

			it("given an invalid truncation factor", function()
				assert.error(function()
					fileTransport({ filepath = "some/file.log", truncationFactor = 0 })
				end, "Invalid truncation factor! Must be between 0 (exclusive) and 1 (inclusive)")

				assert.error(function()
					fileTransport({ filepath = "some/file.log", truncationFactor = -1 })
				end, "Invalid truncation factor! Must be between 0 (exclusive) and 1 (inclusive)")

				assert.error(function()
					fileTransport({ filepath = "some/file.log", truncationFactor = 1.1 })
				end, "Invalid truncation factor! Must be between 0 (exclusive) and 1 (inclusive)")

				assert.no_error(function()
					fileTransport({ filepath = "some/file.log", truncationFactor = 0.9999 })
				end)

				assert.no_error(function()
					fileTransport({ filepath = "some/file.log", truncationFactor = 0.0001 })
				end)
			end)
		end)
	end)

	describe("transports a message to a log file", function()
		---@type file*
		local file
		local originalOpen = io.open

		setup(function()
			io.open = spy.new(function()
				return file
			end)
		end)

		teardown(function()
			io.open = originalOpen
		end)

		before_each(function()
			file = {
				_content = "",
				seek = function()
					return 500
				end,
				write = function(self, text)
					self._content = self._content .. text
				end,
				flush = function() end,
				close = function() end,
				lines = function()
					local i = 5
					return function()
						i = i - 1
						return i > 0 and "text" or nil
					end
				end,
			}
			mock(file)
		end)

		it("by writing to it", function()
			local transport = fileTransport({ filepath = "path/to/file.log" })

			local logMessage = { message = "testing!" }
			transport.log({}, logMessage)

			assert.spy(file.write).called_with(match.is_ref(file), "testing!\n")
		end)

		it("and flushes it", function()
			local transport = fileTransport({ filepath = "path/to/file.log" })

			local logMessage = { message = "testing!" }
			transport.log({}, logMessage)

			assert.spy(file.flush).called(1)
		end)

		describe("and truncates it", function()
			it("when the max file size is exceeded", function()
				local transport = fileTransport({ filepath = "path/to/file.log", softMaxFileSize = 400 })

				local logMessage = { message = "testing!" }
				transport.log({}, logMessage)

				assert.spy(file.close).called(3)
				assert.spy(io.open).called_with("path/to/file.log", "r")
				assert.spy(io.open).called_with("path/to/file.log", "w")
				assert.spy(file.write).called_with(match.is_ref(file), "text\n")
				assert.equal("text\ntext\ntext\ntesting!\n", file._content)
			end)

			it("a specified amount", function()
				local transport =
					fileTransport({ filepath = "path/to/file.log", softMaxFileSize = 400, truncationFactor = 0.8 })

				local logMessage = { message = "testing!" }
				transport.log({}, logMessage)

				assert.spy(file.close).called(3)
				assert.spy(io.open).called_with("path/to/file.log", "r")
				assert.spy(io.open).called_with("path/to/file.log", "w")
				assert.spy(file.write).called_with(match.is_ref(file), "text\n")
				assert.equal("text\ntesting!\n", file._content)
			end)
		end)
	end)
end)
