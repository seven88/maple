local init = require("maple")

describe("logging init module", function()
	---@type Maple.Logger
	local Logger

	setup(function()
		Logger = require("maple.classes.Logger")
		mock(Logger, true)
	end)

	describe("provides a default logger", function()
		it("by proxying it", function()
			assert.equal(Logger.prototype, getmetatable(init.defaultLogger).__index) ---@diagnostic disable-line:undefined-field

			init.defaultLogger.debug = spy.new(function() end)

			init:debug("testing!")
			assert.stub(init.defaultLogger.debug).called_with(init, "testing!")
		end)

		it("that can be overwritten", function()
			local instance = { debug = spy.new(function() end) }
			init.defaultLogger = instance

			assert.equal(instance, init.defaultLogger)

			init:debug("testing!")
			assert.stub(instance.debug).called_with(init, "testing!")
		end)
	end)

	it("allows users to store their custom levels", function()
		init.levels = { 1, 2, 3 }

		assert.same({ 1, 2, 3 }, init.levels)
	end)

	describe("createLogger", function()
		it("creates a new logger", function()
			local config = {}
			init.createLogger(config)

			assert.stub(Logger.new).called_with(config)
		end)
	end)
end)
