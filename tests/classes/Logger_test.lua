local Logger = require("maple.classes.Logger")
local config = require("maple.config")

---@diagnostic disable:missing-fields

describe("Logger Module", function()
	describe("new", function()
		it("throws if no transport is provided", function()
			assert.error(function()
				Logger.new({})
			end, "At least one transport must be provided")
			assert.error(function()
				Logger.new({ transports = {} })
			end, "At least one transport must be provided")
		end)

		it("throws if no formatter is provided", function()
			assert.error(function()
				Logger.new({ transports = { {} } })
			end, "At least one formatter must be provided")
			assert.error(function()
				Logger.new({ transports = { {} }, formatters = {} })
			end, "At least one formatter must be provided")
		end)

		it("throws if levels.levels is not a table", function()
			assert.error(function()
				Logger.new({
					transports = { {} },
					---@diagnostic disable:assign-type-mismatch
					formatters = { {} },
					levels = {
						levels = true,
						names = { "" },
						colors = { "" },
					},
				})
			end, "levels.levels must be a table")
		end)

		it("throws if a levels.names is not a table", function()
			assert.error(function()
				Logger.new({
					transports = { {} },
					---@diagnostic disable:assign-type-mismatch
					formatters = { {} },
					levels = {
						levels = { one = 1 },
						names = true,
						colors = { "" },
					},
				})
			end, "levels.names must be a table")
		end)

		it("throws if a levels.colors is not a table", function()
			assert.error(function()
				Logger.new({
					transports = { {} },
					---@diagnostic disable:assign-type-mismatch
					formatters = { {} },
					levels = {
						levels = { one = 1 },
						names = { "" },
						colors = true,
					},
				})
			end, "levels.colors must be a table")
		end)

		it("throws if a level is a reserved word", function()
			assert.error(function()
				Logger.new({
					transports = { {} },
					---@diagnostic disable:assign-type-mismatch
					formatters = { {} },
					levels = {
						levels = { log = 0 },
						names = { "_log_" },
						colors = { "" },
					},
				})
			end, 'Log level "log" is a reserved word. Please choose another name for the level')
		end)

		it("creates log methods for each log level", function()
			local instance = Logger.new({
				transports = { {} },
				---@diagnostic disable-next-line:assign-type-mismatch
				formatters = { {} },
				levels = {
					levels = { myLevel = 1, anotherLevel = 2 },
					names = { "_myLevel_", "anotherLevel" },
					colors = { "", "" },
				},
			})

			---@diagnostic disable-next-line:undefined-field
			assert.Not.Nil(instance.myLevel)

			local stubbed = stub(instance, "log")
			local meta = {}

			---@diagnostic disable-next-line:undefined-field
			instance:myLevel("wow", meta)
			assert.spy(stubbed).called_with(match.is_ref(instance), 1, "wow", meta)
		end)

		it("creates logF methods for each log level", function()
			local instance = Logger.new({
				transports = { {} },
				---@diagnostic disable-next-line:assign-type-mismatch
				formatters = { {} },
				levels = {
					levels = { myLevel = 1, anotherLevel = 2 },
					names = { "_myLevel_", "anotherLevel" },
					colors = { "", "" },
				},
			})

			---@diagnostic disable-next-line:undefined-field
			assert.Not.Nil(instance.myLevel)

			local stubbed = stub(instance, "logF")
			local meta = {}

			---@diagnostic disable-next-line:undefined-field
			instance:myLevelF(meta, "%s%i", "Winner = ", 2)
			assert.spy(stubbed).called_with(match.is_ref(instance), 1, meta, "%s%i", "Winner = ", 2)
		end)

		it("applies default levels if none given", function()
			local options = { transports = { {} }, formatters = { {} } }

			local l = Logger.new(options)

			assert.equal(config.levels, l.levels.levels)
			assert.equal(config.levelNames, l.levels.names)
			assert.equal(config.levelColors, l.levels.colors)
		end)

		it("will default to displaying all logs", function()
			local options = { transports = { {} }, formatters = { {} } }

			local l = Logger.new(options)

			assert.equal(math.huge, l.level)
		end)

		it("creates a logger instance", function()
			local levels = {
				levels = {
					error = 1,
					warn = 2,
					misc = 3,
				},
				names = {
					"Error",
					"Warn",
					"Misc",
				},
				colors = {
					"",
					"",
					"",
				},
			}
			local options = {
				level = levels.levels.warn,
				levels = levels,
				transports = { {} },
				formatters = { {} },
				shutup = true,
				meta = {},
			}

			local l = Logger.new(options)

			assert.equal(options.level, l.level)
			assert.equal(options.levels, l.levels)
			assert.equal(options.transports, l.transports)
			assert.equal(options.formatters, l.formatters)
			assert.equal(options.shutup, l.shutup)
			assert.equal(options.meta, l.meta)
		end)
	end)

	describe(":log", function()
		it("throws if given metadata is not nil and is not a table", function()
			local formatter = function() end
			local transport = {}
			local logger = {
				level = 0,
				formatters = { formatter },
				transports = { transport },
				meta = {},
			}

			assert.error(function()
				Logger.prototype.log(logger, 0, "", false) ---@diagnostic disable-line: param-type-mismatch
			end, "Provided metadata is not a table")
			assert.no.error(function()
				Logger.prototype.log(logger, 0, "", nil)
			end, "")
		end)

		it("says nothing when asked to shut up", function()
			local formatter = spy.new(function() end)

			Logger.prototype.log({ shutup = true, formatters = { formatter } }, 0, "")

			assert.spy(formatter).called(0)
		end)

		it("calls formatters to format the message", function()
			local formatterA = spy.new(function() end)
			local formatterB = spy.new(function() end)
			local transportA = { log = spy.new(function() end) }

			Logger.prototype.log({
				level = 4,
				formatters = { formatterA, formatterB },
				transports = { transportA },
				meta = { wow = true },
			}, 0, "", { additionalMeta = true })

			assert.spy(formatterA).called(1)
			assert.spy(formatterB).called(1)
		end)

		it("calls transports to print the message", function()
			local formatter = spy.new(function() end)
			local transportA = { log = spy.new(function() end) }
			local transportB = { log = spy.new(function() end) }
			local logger = {
				level = 4,
				formatters = { formatter },
				transports = { transportA, transportB },
				meta = { wow = true },
			}

			Logger.prototype.log(logger, 0, "", { additionalMeta = true })

			local logMessage = { _message = "", message = "", level = 0, meta = { additionalMeta = true, wow = true } }

			assert.spy(transportA.log).called_with(match.is_ref(logger), logMessage)
			assert.spy(transportB.log).called_with(match.is_ref(logger), logMessage)
		end)

		it("aborts logging a message if a transport's level is too low", function()
			local formatter = spy.new(function() end)
			local transport = { log = spy.new(function() end), level = 2 }
			local logger = {
				level = 4,
				formatters = { formatter },
				transports = { transport },
				meta = { wow = true },
			}

			Logger.prototype.log(logger, 3, "", { additionalMeta = true })
			assert.spy(transport.log).called(0)
		end)

		it("aborts logging a message if a formatter returns false", function()
			local formatter = spy.new(function()
				return false
			end)
			local transport = { log = spy.new(function() end) }
			local logger = {
				level = 0,
				formatters = { formatter },
				transports = { transport },
				meta = { wow = true },
			}

			Logger.prototype.log(logger, 0, "", { additionalMeta = true })
			assert.spy(transport.log).called(0)
		end)

		it("supports overriding formatters by a transport", function()
			local formatterA = spy.new(function(_, msg)
				msg.message = true
			end)
			local formatterB = spy.new(function(_, msg)
				msg.message = false
			end)
			local transportA = { log = spy.new(function() end) }
			local transportB = { log = spy.new(function() end), formatters = { formatterB } }
			local logger = {
				level = 0,
				formatters = { formatterA },
				transports = { transportA, transportB },
				meta = { wow = true },
			}

			Logger.prototype.log(logger, 0, "", { additionalMeta = true })

			assert.spy(transportA.log).called_with(
				match.is_ref(logger),
				{ _message = "", message = true, level = 0, meta = { additionalMeta = true, wow = true } }
			)
			assert.spy(transportB.log).called_with(
				match.is_ref(logger),
				{ _message = "", message = false, level = 0, meta = { additionalMeta = true, wow = true } }
			)
		end)

		it("aborts logging a message if an override formatter returns false", function()
			local formatterA = spy.new(function(_, msg)
				msg.message = true
			end)
			local formatterB = spy.new(function()
				return false
			end)
			local transportA = { log = spy.new(function() end) }
			local transportB = { log = spy.new(function() end), formatters = { formatterB } }
			local logger = {
				level = 0,
				formatters = { formatterA },
				transports = { transportA, transportB },
				meta = { wow = true },
			}

			Logger.prototype.log(logger, 0, "", { additionalMeta = true })

			assert.spy(transportA.log).called_with(
				match.is_ref(logger),
				{ _message = "", message = true, level = 0, meta = { additionalMeta = true, wow = true } }
			)
			assert.spy(transportB.log).called(0)
		end)

		it("wraps formatter errors", function()
			local formatter = spy.new(function()
				error("oops!")
			end)
			local transport = { log = spy.new(function() end) }
			local logger = {
				level = 0,
				formatters = { formatter },
				transports = { transport },
				meta = {},
			}

			assert.error_matches(function()
				Logger.prototype.log(logger, 0, "")
			end, "Formatter 1 encountered an error:[^:]+:%d+: oops!")
		end)

		it("wraps transporter errors", function()
			local formatter = spy.new(function() end)
			local transport = { log = spy.new(function()
				error("oops!")
			end) }
			local logger = {
				level = 0,
				formatters = { formatter },
				transports = { transport },
				meta = {},
			}

			assert.error_matches(function()
				Logger.prototype.log(logger, 0, "")
			end, "Transport 1 encountered an error:[^:]+:%d+: oops!")
		end)
	end)

	describe(":logF", function()
		it("wraps string format errors", function()
			local formatterA = spy.new(function() end)
			local formatterB = spy.new(function() end)
			local transportA = { log = spy.new(function() end) }

			local instance = {
				level = 4,
				formatters = { formatterA, formatterB },
				transports = { transportA },
				meta = { stacktrace = true },
				log = spy.new(function() end),
			}

			assert.error_match(function()
				Logger.prototype.logF(instance, 99, {}, "%bbb", "Player #", 8)
			end, '.+\nFormat string: "%%bbb"\nArgs:\n  Player #\n  8')
		end)

		it("formats the given string and calls :log", function()
			local formatterA = spy.new(function() end)
			local formatterB = spy.new(function() end)
			local transportA = { log = spy.new(function() end) }

			local instance = {
				level = 4,
				formatters = { formatterA, formatterB },
				transports = { transportA },
				meta = { wow = true },
				log = spy.new(function() end),
			}

			Logger.prototype.logF(instance, 99, { additionalMeta = true }, "%s%i", "Player #", 8)

			assert.spy(instance.log).called_with(match.is_ref(instance), 99, "Player #8", { additionalMeta = true })
		end)

		it("allows not supplying metadata", function()
			local formatterA = spy.new(function() end)
			local transportA = { log = spy.new(function() end) }

			local instance = {
				level = 4,
				formatters = { formatterA },
				transports = { transportA },
				meta = { success = true },
				log = spy.new(function() end),
			}

			Logger.prototype.logF(instance, 99, "%s%i", "Player #", 8)

			assert.spy(instance.log).called_with(match.is_ref(instance), 99, "Player #8", nil)
		end)
	end)

	describe(":protect", function()
		it("throws if not given a prefix or function", function()
			local logger = {}

			assert.error(function()
				Logger.prototype.protect(logger, {})
			end, "Argument #1 must be an error message prefix or function to call in protected mode")
		end)

		it("throws if not given a function", function()
			local logger = {}

			assert.error(function()
				Logger.prototype.protect(logger, "Some prefix:", {})
			end, "Argument #2 must be a function when providing a error message prefix")
		end)

		it("calls a function in protected mode", function()
			local logger = {
				levels = {
					levels = {
						error = 1,
						warn = 2,
						info = 3,
					},
				},
			}

			local success, a, b = Logger.prototype.protect(logger, function(a, b)
				return a, b
			end, 4, 7)

			assert.True(success)
			assert.equal(4, a)
			assert.equal(7, b)
		end)

		it("prefixes errors with given string", function()
			local errMsg = ""
			local logger = {
				levels = {
					levels = {
						error = 1,
						warn = 2,
						info = 3,
					},
				},
				logF = spy.new(function(_, _, format, err)
					errMsg = string.format(format, err)
				end),
			}

			local success = Logger.prototype.protect(logger, "Custom prefix", function(arg)
				error(arg, 2)
			end, "oh no!")

			assert.False(success)
			assert.equal("Custom prefix oh no!", errMsg)
		end)

		it("handles a protected function erroring", function()
			local errMsg = ""
			local logger = {
				levels = {
					levels = {
						error = 1,
						warn = 2,
						info = 3,
					},
				},
				logF = spy.new(function(_, _, format, err)
					errMsg = string.format(format, err)
				end),
			}

			local success = Logger.prototype.protect(logger, function()
				error("panic!!", 2)
			end)

			assert.False(success)
			assert.equal("Caught error: panic!!", errMsg)
		end)
	end)

	describe(":profile", function()
		local oldClock = os.clock
		local time = 2

		setup(function()
			os.clock = spy.new(function()
				time = time * 2
				return time
			end)
		end)

		teardown(function()
			os.clock = oldClock
		end)

		before_each(function()
			time = 2
		end)

		it("allows profiling the execution time of some code", function()
			local logMsg ---@type string
			local logLevel ---@type Maple.Level
			local logger = {
				logF = spy.new(function(_, level, format, err)
					logLevel = level
					logMsg = string.format(format, err)
				end),
			}

			Logger.prototype.profile(logger, 8, "Took: ")()

			assert.equal(8, logLevel)
			assert.equal("Took: 4.000000s", logMsg)
		end)

		it("logs a default message if one is not given", function()
			local logMsg ---@type string
			local logLevel ---@type Maple.Level
			local logger = {
				logF = spy.new(function(_, level, format, err)
					logLevel = level
					logMsg = string.format(format, err)
				end),
			}

			Logger.prototype.profile(logger, 3)()

			assert.equal(3, logLevel)
			assert.equal("Execution duration: 4.000000s", logMsg)
		end)
	end)

	describe(":child", function()
		it("creates a child logger and adds metadata", function()
			local logger = { someData = false }

			local child = Logger.prototype.child(logger, { num = 53 })

			assert.Not.equal(logger, child)
			local expected = { someData = false, meta = { num = 53 } }
			assert.same(expected, child)
			assert.equal(Logger.prototype, getmetatable(child).__index)
		end)

		it("creates a child logger and overwrites metadata", function()
			local logger = { someData = false, meta = { num = 123, str = "testing" } }

			local child = Logger.prototype.child(logger, { num = 53 })

			assert.Not.equal(logger, child)
			local expected = { someData = false, meta = { num = 53, str = "testing" } }
			assert.same(expected, child)
			assert.equal(Logger.prototype, getmetatable(child).__index)
		end)

		it("creates a child when parent is recursive", function()
			local logger = { someData = false, meta = { num = 53 } }
			logger.meta.recurse = logger.meta

			---@diagnostic disable-next-line:missing-parameter
			local child = Logger.prototype.child(logger)

			assert.Not.equal(logger, child)
			local expected = { someData = false, meta = logger.meta }
			assert.same(expected, child)
			assert.equal(Logger.prototype, getmetatable(child).__index)
		end)
	end)
end)
