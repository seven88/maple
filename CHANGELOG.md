# Changelog

Notable changes to Maple will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.4.1] - 2025-02-06

### Fixed

- Bump _VERSION in [`init.lua`](src/maple/init.lua)

## [v0.4.0] - 2025-02-06

### Fixed
- `maple.formatters.uncolor` is now accessible in rock

## [v0.3.1] - 2025-01-06

### Added

- `uncolor` formatter for removing coloring after a message, preventing color spill into other log messages

## [v0.3.0] - 2024-7-25

### Added

- `Logger.protect` method for executing functions in protected mode and logging any errors
- `Logger.profile` method for performance profiling execution durations

## [v0.2.0] - 2024-05-28

### Added

- `Logger.logF` method for `string.format`-ing log messages
- `Logger.logF` wrapper for each log level

### Changed

- `timestamp` formatter can accept a custom datetime format string

### Fixed

- Increased `stacktrace` formatter call stack depth
- Only deep copy necessary fields for improved performance
- Assert that given metadata is a table or nil

## v0.1.0 - 2024-03-05

### Added

- Initial release of Maple. 🎉

<!-- Version diff links -->
[Unreleased]: https://gitlab.com/seven88/maple/-/compare/0.4.1...HEAD
[v0.4.1]: https://gitlab.com/seven88/maple/-/compare/0.4.0...0.4.1
[v0.4.0]: https://gitlab.com/seven88/maple/-/compare/0.3.1...0.4.0
[v0.3.1]: https://gitlab.com/seven88/maple/-/compare/0.3.0...0.3.1
[v0.3.0]: https://gitlab.com/seven88/maple/-/compare/0.2.0...0.3.0
[v0.2.0]: https://gitlab.com/seven88/maple/-/compare/0.1.0...0.2.0
