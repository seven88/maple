# Included Formatters

## Colors

The colors formatter uses the message's level to index the [logger's](#logger) `levels.colors` table to get the corresponding ANSI color escape string and append it to the message. See [uncolor](#uncolor) to remove coloring following a message to prevent color from bleeding into other log messages.

## Meta

The meta formatter appends the key-value pairs found in a message's `meta` table to the log message. It does not support nested tables.

## Simple

The simple formatter is the go-to and adds a [timestamp](#timestamp-formatter), the message level, and message content. It accepts the below options:

|    Field    | Value Type | Required |                                                                                                                                  |
| :---------: | :--------: | :------: | -------------------------------------------------------------------------------------------------------------------------------- |
| levelLength | `integer`  |    ❌    | The length of the longest level name. Used to add padding around levels to ensure they are all the same length. Defaults to `5`. |

## Stacktrace

The stacktrace formatter uses the built-in `debug` library to append a stacktrace to the message if `meta.stacktrace` is `true`.

## Timestamp

The timestamp formatter uses the built-in `os` library to get the current date-time timestamp and append it to the message.

## Uncolor

Appends ANSI color reset escape string to message to remove coloring from the [colors formatter](#colors), preventing colors from bleeding into other log messages.
