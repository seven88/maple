# Included Transports

## Console

The console transport simply logs messages using `print` to `stdout`. It accepts the same options that all [transports](#transports) do.

## File

The file transport is a bit more complex. It uses the built-in `io` library to open and write to a log file. It will automatically truncate the log file after it exceeds the specified size. On top of accepting the default [transport](#transports) options `table`, it also takes the following:

|      Field       | Value Type | Required |                                                                                                                                                        |
| :--------------: | :--------: | :------: | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
|     filepath     |  `string`  |    ✅    | The path to create the logfile at. e.g. `path/to/myFile.log`. Note that any non-existent directories along the path will not be automatically created. |
| softMaxFileSize  | `integer`  |    ❌    | Target max log file size in bytes. When exceeded, truncating may take place. Default is `50000`                                                        |
| truncationFactor |  `number`  |    ❌    | The percentage of the log file that should be truncated when the max file size is exceeded. e.g. `0.5` (50%), `1` (100%). Defaults to `0.33` (30%)     |
